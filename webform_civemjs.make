core = 7.x
api = 2
libraries[civem-js][type] = "libraries"
libraries[civem-js][download][type] = "file"
libraries[civem-js][download][url] = "https://github.com/downloads/javanto/civem.js/civem-0.0.7.min.js"
