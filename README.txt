CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
Webform Civem js is based on the Civem js https://github.com/javanto/civem.js.
It allows to change HTML 5 validation error messages hardcoded by browsers.

REQUIREMENTS
------------
 * Webform 4.x
 * Libraries
 * Cievem Js library

RECOMMENDED MODULES
-------------------
 * Markdown filter (https://www.drupal.org/project/markdown):
   When enabled, display of the project's README.txt help will be rendered
   with markdown.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
 * Assure that you are using Webform 4.x. http://drupal.org/project/webform
 * Upload/install the Libraries API module. http://drupal.org/project/libraries
 * Create a sites/all/libraries directory on your server.
 * Create a directory within sites/all/libraries named civem-js.
 * Locate/download the Civem js minified and non-minified with names
   civem-min.js and civem.js https://github.com/javanto/civem.js/downloads
 * Enable the Webform Civemjs module.

CONFIGURATION
-------------
 * This module will provides custom fields in the webform components to provide
   the custom validation messages.
 * Edit webform component.
 * You should now see an option in every component of a webform to provide the
   custom messages.

MAINTAINERS
-----------
Current maintainers:
 * Naveen Valecha (naveenvalecha) - https://drupal.org/u/naveenvalecha
