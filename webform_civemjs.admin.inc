<?php

/**
 * @file
 * Configuration file of the Webform Civim Js.
 */

/**
 * Configuration form of Webform Civim Js.
 */
function webform_civemjs_administration_form($form, &$form_state) {
  $form = array();
  $form['webform_civemjs_development_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Debugging Mode.'),
    '#default_value' => variable_get('webform_civemjs_development_mode', FALSE),
    '#description' => t('Enable the debugging mode for development.'),
  );
  return system_settings_form($form, TRUE);
}
